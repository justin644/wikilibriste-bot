[![Latest Release](https://gitlab.com/ayoahha/wikilibriste-bot/-/badges/release.svg)](https://gitlab.com/ayoahha/wikilibriste-bot/-/releases) 
[![License: CC BY-NC-SA 4.0](https://img.shields.io/badge/License-CC%20BY--NC--SA%204.0-lightgrey.svg)](http://creativecommons.org/licenses/by-nc-sa/4.0/)
[![coverage report](https://gitlab.com/ayoahha/wikilibriste-bot/badges/main/coverage.svg)](https://gitlab.com/ayoahha/wikilibriste-bot/-/commits/main)

---

# Bot wikilibriste

Bot d'aide à la navigation du contenu sur [Wikilibriste](https://wikilibriste.fr).

Implémenté en python (3.10+) et déployé sur le [Groupe telegram Wilibriste-Colinu](https://t.me/securite_informatique_libre).

_L'application est générée en package avec setuptools._

### Note

Le bot ne fonctionne qu'avec le token API généré.
Celui-ci n'est pas publié sur le dépôt et n'apparait pas en clair dans l'application.

Afin de le rapatrier :
- Contactez-moi en privé pour initier une communication protégée.

Pour configurer votre environnement :
- Ajoutez un fichier `.env` à ce chemin `~/.config/wkbot`, puis ajoutez ceci dans le fichier :
```bash
API_TOKEN=<le_token>
```

L'application interrogera automatiquement ce fichier lors de son lancement.
Si le fichier n'est pas présent, une erreur sera générée.

## Outils

Le projet utilise les outils suivants :
- la librairie [python-telegram-bot](https://github.com/python-telegram-bot/python-telegram-bot) pour l'API telegram.
- la librairie [pylint](https://pylint.readthedocs.io/en/stable/) pour l'analyse de code statique.
- la librairie [pytest](https://docs.pytest.org/en/7.4.x/contents.html) pour les tests unitaires.
- la sémantique [semver](https://semver.org/lang/fr/) pour le versioning.
- la génération des commits [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/#specification).