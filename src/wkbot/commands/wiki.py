"""
Fichier module pour la commande WIKI.
Permet d'analyser le message en reply et de fournir 
les articles ou tutoriels en lien.

"""
import logging

from telegram import Update, InlineKeyboardMarkup
from telegram.ext import ContextTypes

from ..utils.helpers import (
    get_reply_id,
    get_keywords,
    generate_response,
    remove_urls,
    try_to_delete
)
from ..utils.constants import (
    GROUPE_USERNAME,
    GROUPE_DEV_USERNAME,
    MSG_NO_KEYWORDS,
    MSG_NO_REPLY_MSG
)

logger = logging.getLogger(__name__)


async def wiki_cmd(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """Commande pour renvoi vers les articles du wiki"""

    message = update.effective_message
    chat_id = update.effective_chat.id

    if message.from_user:
        thread_id = message.message_thread_id
        message_id = message.message_id
        user_id = message.from_user.id
        user_name = message.from_user.username
        logger.info(
            "/wiki cmd lancée dans le chat id: %s in topic %s by %s - %s",
            chat_id,
            thread_id,
            user_id,
            user_name,
        )
    else:
        logger.info(
            "/wiki cmd: %s - %s. ",
            "ID inconnu",
            "Username inconnu!"
        )
        logger.debug(update)
        return

    if message.chat.username in (GROUPE_USERNAME, GROUPE_DEV_USERNAME):
        reply = message.reply_to_message
        if reply and reply.text:
            # Supprime toutes les urls dans les messages pour ne pas interférer dans l'analyse
            message_text = remove_urls(reply.text)
            issued_reply = get_reply_id(update)

            # Récupère les informations de l'utilisateur
            if reply.from_user:
                if reply.from_user.username:
                    name = "@" + reply.from_user.username
                else:
                    name = reply.from_user.first_name
            else:
                name = "Inconnu"

            # Appelle la fonction d'extraction des mots-clés à partir du message
            keywords, buttons = get_keywords(message_text)
            if keywords:
                response = name + "\n"
                # Génère la réponse finale suivant les mots clés
                response += generate_response(keywords)

                if buttons:
                    # Disposition en lignes (et non sur une seule ligne)
                    button_rows = [[button] for button in buttons]
                    if thread_id == message_id-1:
                        await message.reply_text(
                            response,
                            reply_markup=InlineKeyboardMarkup(button_rows),
                            quote=False,
                            reply_to_message_id=issued_reply,
                        )
                    else:
                        await message.reply_text(
                            response,
                            reply_markup=InlineKeyboardMarkup(button_rows),
                            quote=False,
                            message_thread_id=thread_id,
                            reply_to_message_id=issued_reply,
                        )
                    context.application.create_task(try_to_delete(message), update=update)
                else:
                    # button_rows = None
                    if thread_id == message_id-1:
                        await message.reply_text(
                            response,
                            quote=False,
                            reply_to_message_id=issued_reply,
                        )
                    else:
                        await message.reply_text(
                            response,
                            quote=False,
                            message_thread_id=thread_id,
                            reply_to_message_id=issued_reply,
                        )
                    context.application.create_task(try_to_delete(message), update=update)
            else:
                response = MSG_NO_KEYWORDS
                if thread_id == message_id-1:
                    await message.reply_text(
                        response,
                        quote=False,
                        reply_to_message_id=issued_reply,
                    )
                else:
                    await message.reply_text(
                        response,
                        quote=False,
                        message_thread_id=thread_id,
                        reply_to_message_id=issued_reply,
                    )
                context.application.create_task(try_to_delete(message), update=update)
        else:
            response = "@" + user_name
            response += MSG_NO_REPLY_MSG
            if thread_id == message_id-1:
                await message.reply_text(
                    response,
                    quote=False,
                )
            else:
                await message.reply_text(
                    response,
                    quote=False,
                    message_thread_id=thread_id,
                )
            context.application.create_task(try_to_delete(message), update=update)
    else:
        await message.reply_text(
            "Hmm. Vous n'êtes pas dans le groupe Numérique Libre."
            "Je ne peux donc pas vous répondre."
        )
