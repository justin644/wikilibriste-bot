"""
Fichier module pour la commande DON.

"""
import logging

from telegram import Update
from telegram.ext import ContextTypes

from ..utils.helpers import (
    get_reply_id,
    try_to_delete,
)

from ..utils.constants import (
    GROUPE_USERNAME,
    GROUPE_DEV_USERNAME,
    MSG_DON,
)

logger = logging.getLogger(__name__)


async def don_cmd(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """Envoie les informations suite à la commande /don"""

    message = update.effective_message
    chat_id = update.effective_chat.id

    if not message.from_user:
        logger.info(
            "/don cmd lancée dans le chat id: %s by %s - %s",
            chat_id,
            "ID inconnu",
            "Username inconnu!"
        )
        logger.debug(update)
        await message.reply_text(
            "🛑 Erreur détectée dans le message joint."
            "Veuillez vous rapprocher des administrateurs."
        )
        context.application.create_task(try_to_delete(message), update=update)
        return

    if message.chat.username in (GROUPE_USERNAME, GROUPE_DEV_USERNAME):
        reply = message.reply_to_message
        if reply and reply.text:
            issued_reply = get_reply_id(update)
            # Récupère les informations de l'utilisateur
            if reply.from_user:
                if reply.from_user.username:
                    name = "@" + reply.from_user.username
                else:
                    name = reply.from_user.first_name
            else:
                name = "Inconnu"

            response = name + "\n"
            # Génère la réponse finale
            response += MSG_DON
            await message.reply_text(
                response,
                quote=False,
                message_thread_id=message.message_thread_id,
                reply_to_message_id=issued_reply,
            )
        else:
            user_id = message.from_user.id
            user_name = message.from_user.username
            if message.message_thread_id:
                logger.info(
                    "/don cmd lancée dans le chat id: %s sujet %s by %s - %s",
                    chat_id,
                    message.message_thread_id,
                    user_id,
                    user_name
                )
                await message.reply_text(
                    MSG_DON,
                    quote=False,
                    message_thread_id=message.message_thread_id
                )
            else:
                logger.info(
                    "/don cmd lancée dans le chat id: %s sujet %s by %s - %s",
                    chat_id,
                    user_id,
                    "General/Divers",
                    user_name
                )
                await message.reply_text(
                    MSG_DON,
                    quote=False,
                )

        context.application.create_task(try_to_delete(message), update=update)
    else:
        await message.reply_text(
            "Hmm. Vous n'êtes pas dans le groupe Numérique Libre."
            "Je ne peux donc pas vous répondre."
        )
