"""
Fichier des helpers/fonctions utiles.

"""
import os
import re
import json

from typing import Tuple, List, Optional

from telegram import (
    Update,
    InlineKeyboardButton,
    Message
)
from telegram.ext import ApplicationHandlerStop, ContextTypes
from telegram.error import BadRequest, Forbidden

from .constants import (
    MSG_FIRST,
    MSG_SECOND,
    APK,
    PATTERNS_URL,
)


async def raise_app_handler_stop(_: Update, __: ContextTypes.DEFAULT_TYPE) -> None:
    """Gestion de l'arrêt des handlers."""

    raise ApplicationHandlerStop


async def leave_chat(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """Routine pour quitter les canaux requis."""

    context.application.create_task(update.effective_chat.leave(), update=update)
    raise ApplicationHandlerStop


async def try_to_delete(message: Message) -> bool:
    """Routine de suppression d'un message."""

    try:
        return await message.delete()
    except (BadRequest, Forbidden):
        return False


def build_command_list() -> List[Tuple[str, str]]:
    """Indique les commandes disponibles."""

    base_commands = [
        ("start", "Ping le bot."),
        ("rules", "Affiche les règles du groupe."),
        ("wiki", "Renvoi vers le wiki en fonction du message."),
        ("gafam", "Affiche les objectifs du groupe vs GAFAM/BATX."),
        ("don", "Comment nous faire un don ?"),
    ]
    return base_commands


def load_json_file(filename: str) -> dict:
    """Charge les fichiers JSON."""

    here = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    file_path = os.path.join(here, filename)
    with open(file_path, encoding="utf-8") as json_file:
        return json.load(json_file)


def remove_urls(msg: str) -> str:
    """Détecte si le message contient des URLs et les supprime si c'est le cas."""

    return re.sub(PATTERNS_URL, '', msg, flags=re.IGNORECASE)


def get_reply_id(update: Update) -> Optional[int]:
    """Permet d'extraire l'id d'un message en reply."""

    if update.effective_message and update.effective_message.reply_to_message:
        return update.effective_message.reply_to_message.message_id

    return None


def get_keywords(message: str) -> Tuple[list, List[InlineKeyboardButton]]:
    """Trouve l'ensemble des sujets supportés mentionnés dans un message."""

    keywords = set()
    url_button_data = {}

    for keyword, data in load_json_file("keywords.json").items():
        if re.search(data['regex'], message, re.IGNORECASE):
            keywords.add(keyword)
            # Si l'URL n'a pas déjà été ajoutée, ajouter l'URL à l'ensemble, avec son label
            for button_data in data.get('buttons', []):
                url = button_data.get('url')
                label = button_data.get('label')
                if label:
                    if 'tutoriels' in url:
                        label = f"Tutoriel : {label}"
                    else:
                        label= f"Article : {label}"
                if url and label:
                    url_button_data[label] = url

    buttons = [InlineKeyboardButton(label, url=url) for label, url in url_button_data.items()]

    return list(keywords), buttons


def generate_response(keywords: list) -> str:
    """Génère la réponse des mots-clés."""

    response = f"{MSG_FIRST}<b>{', '.join(keywords)}</b>"
    # Cas spécifique apk : ajout d'un laïus sur le téléchargement d'exécutables
    if 'apk' in keywords:
        response += APK

    response += f"{MSG_SECOND}"
    return response
