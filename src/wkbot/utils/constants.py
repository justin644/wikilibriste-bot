"""
Constantes.

"""
ARROW_CHARACTER = "➜"
GROUPE_USERNAME = "securite_informatique_libre"
GROUPE_DEV_USERNAME = "wkbot_dev"
GROUPE_CHAT_ID = "@" + GROUPE_USERNAME
DEV_CHAT_ID = -1002124940108
LOGS_CHAT_ID = -1001907596326
BOT_CHANNEL_CHAT_ID = 1464722139
ALLOWED_USERNAMES = (GROUPE_USERNAME, GROUPE_DEV_USERNAME)
ALLOWED_CHAT_IDS = {
    BOT_CHANNEL_CHAT_ID,    # wikilibristeBot chat
    LOGS_CHAT_ID,           # logs chat
}
SELF_BOT_NAME = "wikilibristBot"
WIKI_URL = "https://wikilibriste.fr"
RATE_LIMIT_SPACING = 4
PATTERNS_URL = (
    r'\b((?:https?:\/\/|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]'
    r'{2,4}\/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s('
    r')<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+'
    r'\)))*\)|[^\s`!()\[\]{};:\'".,<>?«»“”‘’]))')

# Messages
MSG_NO_KEYWORDS = """🤖 Aucun Mot-clé détecté 🤖
N'hésitez pas à vérifier dans <a href="https://wikilibriste.fr">le wiki</a>.
"""
MSG_FIRST = """🤖 Mots-clés détectés 🤖
"""
MSG_SECOND = """

💡 Avez-vous jeté un oeil sur ces articles du wiki :
"""
APK = """\n\n🛑 Concernant les apk et plus généralement tous les fichiers 'exécutables' : nous
recommandons d'éviter de télécharger ce type de fichiers (apk, exe, msi...)
en dehors de magasins d'applications. En effet, le téléchargement de fichiers
sur des sites tiers (type apkpure, etc.) ou d'applications (whatsapp, telegram, etc.)
n'est pas une bonne pratique et peut mener à un téléchargement d'un malware !
"""
GROUPE_RULES = """
<b>Veuillez respecter le thème du groupe</b> :
- Les licences libres (logiciels, matériels, outils, ...).
- La sécurité, résilience, hygiène numérique avec des outils libres ou résilients, éthiques, respectueux de la vie privée, ...
- Les réseaux décentralisés, fédérés, et l'interopérabilité et neutralité du net.

<b>Règles importantes :</b>
- Nous ne sommes pas un groupe de "Nouvelles Sociétales", mais de Nouvelles Techniques, liées au numérique.
- Merci de ne pas poster de nouvelles anxiogènes et/ou avec un fort accent politique. D'autres groupes exsitent pour cela.
- Merci de ne pas poster de Liens sans description.

⚠️ <i>Nous nous réservons le droit de modérer tout message hors sujet, non lié au technique, irrespectueux ou illégal.</i>

🛑 <b>Merci de ne pas partager de fichiers exécutables (.apk, .exe, .jar, .zip...)</b>
Merci d'envoyer plutôt le lien vers le dépôt ou le code source directement.
Il est recommandé de télécharger les fichiers APK sur F-droid (ou équivalent) ou Aurora store.

🐧 Le lien du wiki :
<a href="https://wikilibriste.fr">Wikilibriste - Wiki</a>
🐧 Le lien du groupe Telegram :
<a href="https://t.me/securite_informatique_libre">Wikilibriste : Numérique Éthique et Résilient</a>
🐧 Canal d'information lié aux outils :
<a href="https://t.me/outils_libres">Outils libres</a>


💡 N'hésitez pas à faire une recherche sur le groupe (avec la 🔍 dans la barre en haut) avec le mot clé correspondant à votre demande avant de poser votre question.
Vous trouverez sûrement des dizaines de réponses !

ℹ️ D'autres groupes permettent d'échanger sur d'autres sujets : 
<a href="https://t.me/partage_citoyen_france">Partage citoyen France</a>

🍁 <b>L'équipe d'administrateurs se fera un plaisir de répondre à toute demande.</b>
"""
GAFAM_RULES="""
✅ <b>Notre groupe traite toutes les questions relatives aux outils sous licences Libre ou à minima Open Source, et respectueux de votre vie privée.</b>
Voir <a href="https://wikilibriste.fr/fr/glossaire#open-source">Libre/Open Source</a>.

🛑  <b>Notre groupe ne traite pas des outils privateurs et en particulier ceux liés aux <a href="https://wikilibriste.fr/fr/glossaire#gafam">GAFAM/BATX</a>.</b>
L'éthique de ces entreprises ne correspond pas à nos valeurs!

👉 Si vous êtes intéressés pour vous libérer, n'hésitez pas à poser vos questions ici.
👉 Sinon, vous pouvez vous diriger vers d'autres canaux, comme :
<a href="https://t.me/helpinformatique">helpinformatique</a>

💡 N'hésitez pas à faire une recherche sur le groupe (avec la 🔍 dans la barre en haut) avec le mot clé correspondant à votre demande avant de poser votre question.
Vous trouverez sûrement des dizaines de réponses !

🍁 <b>L'équipe d'administrateurs se fera un plaisir de répondre à toute demande.</b>
"""
MSG_NO_REPLY_MSG = """
🤖 Aucun message lié avec la commande 🤖
Merci de répondre à un message du groupe pour que je puisse l'analyser.
"""
MSG_DON = """
✅ <b>Voici toutes les informations pour nous faire un don.</b>

Monnaies acceptées :
- Euros
- June
- Bitcoin (BTC - <b>A venir</b>)
- Monero (XMR - <b>A venir</b>)

Il est possible également d'effectuer des échanges de services.
💡 N'hésitez pas à contacter l'équipe.


🐧 Le lien de la page du wiki :
<a href="https://wikilibriste.fr/fr/contribution">Wikilibriste - Soutien</a>
🐧 Le lien de la page Open Collective (don en Euros):
<a href="https://opencollective.com/wikilibriste?language=fr">Wikilibriste - Open Collective</a>
🐧 Le lien de la page Liberapay (don en Euros):
<a href="https://fr.liberapay.com/wikilibriste/">Wikilibriste - Liberapay</a>

🍁 <b>L'équipe vous remercie infiniment.</b>
"""
